FROM debian:bullseye-slim
ENV NGINX_VERSION 1.19.1
RUN useradd nginx && usermod -aG nginx nginx
ENV RUSTUP_HOME=/usr/local/rustup \
    CARGO_HOME=/usr/local/cargo \
    PATH=/usr/local/cargo/bin:$PATH \
    RUST_VERSION=1.44.1

ARG BUILD_DEPS="build-essential \
                git \
                clang-10 \
                llvm-10 \
                llvm-10-dev \
                lld-10 \
                cmake \
                golang \
                curl \
                wget \
                libpcre3 \
                libpcre3-dev \
                zlib1g-dev \
                libxml2-dev \
                libxslt1-dev \
                libgd-dev \
                libgeoip-dev \
                libperl-dev"

RUN echo "Install Build Deps...." && \
    apt update > /dev/null && \
    apt install apt-utils -y > /dev/null && \
    # Build Deps
    DEBIAN_FRONTEND=noninteractive apt install ${BUILD_DEPS} -y > /dev/null && \
    curl -sLo rustup-init "https://static.rust-lang.org/rustup/dist/x86_64-unknown-linux-gnu/rustup-init" && \
    chmod +x rustup-init && \
    ./rustup-init -y --no-modify-path --profile minimal --default-toolchain $RUST_VERSION > /dev/null && \
    rm rustup-init && \
    chmod -R a+w $RUSTUP_HOME $CARGO_HOME && \
    # Run Deps
    echo "Install Run Deps....." && \
    DEBIAN_FRONTEND=noninteractive apt install geoip-database \
                                               iproute2 \
                                               libatm1 \
                                               libgeoip1 \
                                               libmnl0 \
                                               libxml2 \
                                               libpcre3 \
                                               zlib1g \
                                               libxslt1.1 \
                                               libgd3 \
                                               libperl5.30 -y > /dev/null && \
    # Link LLD-10 to LLD, coz -fuse-ld=lld doesnt work otherwise.
    ln -s /usr/bin/lld-10 /usr/bin/lld && \
    ln -s /usr/bin/ld.lld-10 /usr/bin/ld.lld && \
    # Lets create a place to work, so that we can wipe it out later!
    mkdir /work && \
    cd /work && \
    # Pull Nginx Tarball from mainline
    echo "Fetching NGINX Tarball For NGINX-${NGINX_VERSION}" && \
    curl -sLo nginx-${NGINX_VERSION}.tar.gz http://nginx.org/download/nginx-${NGINX_VERSION}.tar.gz && \
    tar -xf nginx-${NGINX_VERSION}.tar.gz && \
    # Quiche+BoringSSL for HTTP/3
    echo "Cloning Quiche from Cloudflare Git...." && \
    git clone --recursive https://github.com/cloudflare/quiche && \
    # Apply Cloudflare Nginx patches, made esp for 1.16 branch, but tested working fine on 1.19.1
    cd /work/nginx-${NGINX_VERSION} && patch -p01 < ../quiche/extras/nginx/nginx-1.16.patch && \
    # Boom, lets get it compiling!
    echo "Starting Build!" && \
    ./configure --prefix=/etc/nginx \ 
            --sbin-path=/usr/sbin/nginx \ 
            --modules-path=/usr/lib/nginx/modules \ 
            --conf-path=/etc/nginx/nginx.conf \
            --error-log-path=/var/log/nginx/error.log \
            --pid-path=/var/run/nginx.pid \
            --lock-path=/var/run/nginx.lock \
            --user=nginx \
            --group=nginx \
            --build=baalajimaestro \
            --builddir=nginx-${NGINX_VERSION} \
            --with-select_module \
            --with-poll_module \
            --with-threads \
            --with-file-aio \
            --with-http_ssl_module \
            --with-http_v2_module \
            --with-http_v3_module \
            --with-http_realip_module \
            --with-http_addition_module \
            --with-http_xslt_module \
            --with-http_image_filter_module \
            --with-http_geoip_module \
            --with-http_sub_module \
            --with-http_dav_module \
            --with-http_flv_module \
            --with-http_mp4_module \
            --with-http_gunzip_module \
            --with-http_gzip_static_module \
            --with-http_auth_request_module \
            --with-http_random_index_module \
            --with-http_secure_link_module \
            --with-http_degradation_module \
            --with-http_slice_module \
            --with-http_stub_status_module \
            --with-http_perl_module \
            --with-perl=/usr/bin/perl \
            --http-log-path=/var/log/nginx/access.log \
            --http-client-body-temp-path=/var/cache/nginx/client_temp \
            --http-proxy-temp-path=/var/cache/nginx/proxy_temp \
            --http-fastcgi-temp-path=/var/cache/nginx/fastcgi_temp \
            --http-uwsgi-temp-path=/var/cache/nginx/uwsgi_temp \
            --http-scgi-temp-path=/var/cache/nginx/scgi_temp \
            --with-mail \
            --with-mail_ssl_module \
            --with-stream \
            --with-stream_ssl_module \
            --with-stream_realip_module \
            --with-stream_geoip_module \
            --with-stream_ssl_preread_module \
            --with-compat \
            --with-pcre-jit \
            --with-openssl=../quiche/deps/boringssl \
   	        --with-quiche=../quiche \
            --with-cc="clang-10" \
            # We need to ignore -W-conditional-uninitialized since applying the quiche patch introduced warnings which cant be fixed at buildtime.
            --with-cc-opt="-O3 -Wno-conditional-uninitialized" \
            --with-ld-opt="-flto=thin -fuse-ld=lld" > /dev/null && \
            echo "Building NGINX! Please wait a while~~" && \
            make -j$(nproc) > /dev/null && \
            echo "Your Build is done, we are installing NGINX~" && \
            make install > /dev/null && \
            echo "Cleaning Up!" && \
            apt remove clang-10 \
                llvm-10 \
                llvm-10-dev \
                lld-10 \
                cmake \
                golang \
                wget \
                build-essential \
                libpcre3-dev \
                zlib1g-dev \
                libxml2-dev \
                libxslt1-dev \
                libgd-dev \
                libgeoip-dev \
                libperl-dev \
                git \
                apt-utils -y > /dev/null && \
            apt autoremove -y > /dev/null && \
            apt-get remove --purge --auto-remove -y && \
            yes | rustup self uninstall && \
            # if we have leftovers from building, let's purge them (including extra, unnecessary build deps)
            apt-get purge -y --auto-remove && \
            rm -rf /work /etc/apt/sources.list.d/temp.list && \
            apt clean && \
            # forward request and error logs to docker log collector
            ln -sf /dev/stdout /var/log/nginx/access.log && \
            ln -sf /dev/stderr /var/log/nginx/error.log && \
            # create a docker-entrypoint.d directory
            mkdir /docker-entrypoint.d

COPY docker-entrypoint.sh /
COPY 10-listen-on-ipv6-by-default.sh /docker-entrypoint.d
COPY 20-envsubst-on-templates.sh /docker-entrypoint.d
RUN chmod 777 /docker-entrypoint.sh && \
    chmod 777 /docker-entrypoint.d/10-listen-on-ipv6-by-default.sh && \
    chmod 777 /docker-entrypoint.d/20-envsubst-on-templates.sh && \
    mkdir -p /var/cache/nginx

ENTRYPOINT ["/docker-entrypoint.sh"]
EXPOSE 80
EXPOSE 443
STOPSIGNAL SIGTERM
CMD ["nginx", "-g", "daemon off;"]